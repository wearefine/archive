#!/usr/bin/env bash

date=`date +%F`

if [ $# -eq 0 ]
  then
    echo "Please provide the name of the project you are going to archive"
    exit 1
fi

mkdir -p $1/{db,assets,configs,src,src/repo,src/repo,src/server/code,src/server/logs}

cat > $1/README.md <<EOF
a FINE project archive
==================
---
#### Name: $1
#### Date: $date
---

\`\`\`
├── README.md
├── assets
├── configs
├── db
└── src
    ├── repo
    └── server
        ├── code
        └── logs
\`\`\`
- *assets:* All dynamic assets and user uploads. 
- *configs:* Passenger and Apache configuration files.
- *db:* Database dump. This is typically the site level db but could be a copy of the admin db for a multisite setup.
- *src*
  - *repo:* The source code from the master branch.
  - *server*
    - *code:* The last deployed code on the server as it stood prior to archival.
    - *logs:* Passenger or PHP logs.
EOF
