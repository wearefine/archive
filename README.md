a FINE project archive
==================
---
#### Name: ok
#### Date: 2017-07-07
---

```
├── README.md
├── assets
├── configs
├── db
└── src
    ├── repo
    └── server
        ├── code
        └── logs
```
- *assets:* All dynamic assets and user uploads. 
- *configs:* Passenger and Apache configuration files.
- *db:* Database dump. This is typically the site level db but could be a copy of the admin db for a multisite setup.
- *src*
  - *repo:* The source code from the master branch.
  - *server*
    - *code:* The last deployed code on the server as it stood prior to archival.
    - *logs:* Passenger or PHP logs.

